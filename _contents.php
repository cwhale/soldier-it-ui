<?php
$list = array(
    ['', 'APP'],
    ['index.php', '首页', ''],

    ['', '大屏端'],
    ['index.php', '* 首页', ''],
    ['login.php', '* 登录', ''],
    ['map-dynamic.php', '*动态地图', ''],
    ['map-static.php', '*静态地图', ''],
    ['meeting.php', '*多人临时会议', ''],
    ['urgency.php', '*险情处理', ''],
    ['plan.php', '*应急预案1', ''],
    ['plan-edit.php', '*应急预案2', ''],
    ['plan-deal.php', '*应急预案3', ''],
);
?>

<style>
    table {
        border-collapse: collapse;
    }
    td, th {
        text-align: left;
        font-weight: normal;
        border: 1px solid black;
        padding: 3px 5px;
        font-size: 14px;
    }
</style>

<table>
    <?php foreach ($list as $row) { ?>
        <tr <?php if($row[1][0] === '*') echo 'style="color: #00BB00;"'?>>
            <th <?php if(sizeof($row) <= 2)
                echo 'colspan="3" style="border: 0;padding-top: 15px;font-size: 18px; font-weight:bold;"'?>><?php echo $row[1]; ?></th>
            <?php if(sizeof($row) > 2) {?>
            <td>
                <a target="mobile" href="<?php echo $row[0]; ?>"><?php echo $row[0]; ?></a>
            </td>
            <td><?php echo @$row[2]; ?></td>
            <?php }?>
        </tr>
    <?php } ?>
</table>
