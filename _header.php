<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta id="viewport" name="viewport"
          content="width=device-width,initial-scale=1.0,
                maximum-scale=1.0,user-scalable=no">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="lib/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="lib/OwlCarousel2/assets/owl.carousel.min.css" />
<!--    <link rel="stylesheet" href="lib/OwlCarousel2/assets/owl.theme.default.min.css" />-->
    <link rel="stylesheet" href="lib/Font-Awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script type="text/javascript" src="lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="lib/OwlCarousel2/owl.carousel.min.js"></script>
</head>
<body>