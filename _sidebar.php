<div id="sidebar">
    <ul class="sidebar-list">
        <!-- 加上active类以获得选中效果 -->
        <li class="sidebar-item menu-index active">
            <a href="javascript:"><!--首页--></a>
        </li>
        <li class="sidebar-item menu-static-map">
            <a href="javascript:"><!--静态地图--></a>
        </li>
        <li class="sidebar-item menu-dynamic-map">
            <a href="javascript:"><!--动态地图--></a>
        </li>
        <li class="sidebar-item menu-plan">
            <a href="javascript:"><!--应急预案--></a>
        </li>
        <li class="sidebar-item menu-urgency">
            <a href="javascript:"><!--险情处理--></a>
        </li>
    </ul>
</div>
