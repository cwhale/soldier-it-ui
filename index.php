<?php include '_header.php'; ?>

    <div id="page" class="page-index">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">险情通知以下省略1000字</div>
                <div class="block-info">监控列表</div>
                <div class="content-body">
                    <div class="camera-menu">
                        <ul class="menu-list">
                            <li class="menu-item">
                                <a href="javascript:">小甜甜摄像</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">王昭君摄像头</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">东北角峡谷</a>
                            </li>
                            <!-- 加上active类以获得选中效果 -->
                            <li class="menu-item active">
                                <a href="javascript:">梦琪摄像</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">庄周摄像</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">甄姬摄像头</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">摄像头名称</a>
                            </li>
                            <li class="menu-item">
                                <a href="javascript:">王者峡谷</a>
                            </li>
                        </ul>
                    </div>
                    <div class="block-video">
                        <iframe class="video"></iframe>
                    </div>
                </div>
            </div>
        </div>

        <div id="dialog-change-password" class="dialog">
            <div class="dialog-window">
                <div class="dialog-header">
                    修改密码
                </div>
                <div class="dialog-body">
                    <div class="row">
                        <div class="label">原密码</div>
                        <div class="control">
                            <input type="password" name="old" placeholder="请输入原密码"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">新密码</div>
                        <div class="control">
                            <input type="password" name="new" placeholder="请输入新密码"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">新密码</div>
                        <div class="control">
                            <input type="password" name="new2" placeholder="请输入新密码"/>
                        </div>
                    </div>
                    <div class="row-action">
                        <a class="btn btn-cancel">取消</a>
                        <a class="btn btn-submit">确定</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
