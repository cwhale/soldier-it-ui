<?php include '_header.php'; ?>

    <div id="page" class="page-login">

        <div class="layer-background">
            <div class="content">
                <div class="block-title"></div>
                <div class="form">
                    <form action="">
                        <div class="form-row">
                            <input type="text" placeholder="账号"/>
                        </div>
                        <div class="form-row">
                            <input type="password" placeholder="密码"/>
                        </div>
                        <div class="action">
                            <a class="btn-login" href="javascript:">登录</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
