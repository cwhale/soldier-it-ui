<?php include '_header.php'; ?>

    <div id="page" class="page-map-dynamic">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content">
                <div class="section-detail">
                    <form action="">
                        <div class="block-subtitle">动态地图</div>
                        <div class="block-select">
                            <label class="check-widget">
                                <input type="checkbox"/>
                                <div class="icon"></div>
                            </label>
                            <div class="item-group">
                                <div class="label">全部分组</div>
                                <select class="group-select" title>
                                    <option>全部分组</option>
                                    <option>第一分组</option>
                                    <option>第二分组</option>
                                    <option>第三分组</option>
                                </select>
                            </div>
                            <label class="check-widget">
                                <input type="checkbox"/>
                                <div class="icon"></div>
                            </label>
                            <div class="item-group">
                                <div class="label">全部分组</div>
                                <select class="group-select" title>
                                    <option>全部分组</option>
                                    <option>第一分组</option>
                                    <option>第二分组</option>
                                    <option>第三分组</option>
                                </select>
                            </div>
                        </div>
                        <div class="block-name">
                            <label class="check-widget">
                                <input type="checkbox"/>
                                <div class="icon"></div>
                            </label>
                            <div class="label">姓名</div>
                            <div class="input input-name">
                                <input type="text" class="text-input" placeholder="请输入"/>
                            </div>
                        </div>
                        <div class="block-duration">
                            <div class="label">时间</div>
                            <div class="input input-time">
                                <input type="datetime" class="datetime-input" placeholder="开始时间"/>
                            </div>
                            <div class="separator"></div>
                            <div class="input input-time">
                                <input type="datetime" class="datetime-input" placeholder="结束时间"/>
                            </div>
                            <a class="btn btn-search" href="javascript:">搜索</a>
                        </div>
                        <div class="block-map">
                            <ul class="soldier-list">
                                <!-- 根据style的top和left动态渲染定位 -->
                                <!-- 根据active类控制弹出框是否显示 -->
                                <li class="soldier-item active"
                                    style="top: 50%; left: 50%">
                                    <a class="avatar" href="javascript:">
                                        <img/>
                                    </a>
                                    <div class="soldier-detail">
                                        <div class="row row-name">
                                            <span class="name">张小龙</span>
                                            23岁
                                        </div>
                                        <div class="row row-title">
                                            职位：指挥长
                                        </div>
                                        <div class="row row-skill">
                                            技能：驾车、急救
                                        </div>
                                        <div class="row row-introduction">
                                            详细介绍：张小龙年龄23职位指挥长，技能驾车急救，反应灵敏洞察力强。
                                        </div>
                                        <a href="javascript:" class="btn btn-talk">视频通话</a>
                                    </div>
                                </li>
                                <li class="soldier-item"
                                    style="top:50%; left: 20%">
                                    <a class="avatar" href="javascript:">
                                        <img/>
                                    </a>
                                    <div class="soldier-detail">
                                        <div class="row row-name">
                                            <span class="name">打真军</span>
                                            23岁
                                        </div>
                                        <div class="row row-title">
                                            职位：指挥长
                                        </div>
                                        <div class="row row-skill">
                                            技能：驾车、急救
                                        </div>
                                        <div class="row row-introduction">
                                            详细介绍：张小龙年龄23职位指挥长，技能驾车急救，反应灵敏洞察力强。
                                        </div>
                                        <a href="javascript:" class="btn btn-talk">视频通话</a>
                                    </div>
                                </li>
                                <li class="soldier-item"
                                    style="top:50%; left: 70%">
                                    <a class="avatar" href="javascript:">
                                        <img/>
                                    </a>
                                    <div class="soldier-detail">
                                        <div class="row row-name">
                                            <span class="name">陈二蛋</span>
                                            23岁
                                        </div>
                                        <div class="row row-title">
                                            职位：指挥长
                                        </div>
                                        <div class="row row-skill">
                                            技能：驾车、急救
                                        </div>
                                        <div class="row row-introduction">
                                            详细介绍：张小龙年龄23职位指挥长，技能驾车急救，反应灵敏洞察力强。
                                        </div>
                                        <a href="javascript:" class="btn btn-talk">视频通话</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="section-property">
                    <!--                    <div class="block-camera">-->
                    <!--                        <div class="header">摄像头名称</div>-->
                    <!--                        <div class="thumbnail">-->
                    <!--                            <img class="thumbnail-image"/>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!-- 通过collapsed标签是否存在改变折叠状态 -->
                    <div class="block-tasks collapsed">
                        <div class="header">
                            附近民兵
                            <a class="btn btn-meeting">多人临时会议</a>
                        </div>
                    </div>
                    <div class="block-tasks collapsed">
                        <div class="body">
<!--                            <div class="dropdown-bar">-->
<!--                                <div class="label">富士山体滑坡</div>-->
<!--                            </div>-->
                            <div class="dropdown-body">
                                <table class="table-content">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>联系方式</th>
                                        <th>职位</th>
                                        <th>技能</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    <tr>
                                        <td>紫小鲸</td>
                                        <td>13800138000</td>
                                        <td>指挥员</td>
                                        <td>驾车</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
