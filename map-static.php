<?php include '_header.php'; ?>

    <div id="page" class="page-map-static">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content">
                <div class="section-detail">
                    <form action="">
                        <div class="block-subtitle">静态地图</div>
                        <div class="block-select">
                            <div class="item-group">
                                <div class="label">信息查询</div>
                                <select class="group-select" title>
                                    <option>全部分组</option>
                                    <option>第一分组</option>
                                    <option>第二分组</option>
                                    <option>第三分组</option>
                                </select>
                            </div>
                        </div>
                        <div class="block-map">
                            <div class="block-legend">
                                <div class="label">全部分组</div>
                                <div class="menu">
                                    <ul class="menu-list">
                                        <li class="menu-item">
                                            <label>
                                                <input type="checkbox"/>
                                                <span class="check-icon"></span>
                                                乡镇介绍
                                            </label>
                                        </li>
                                        <li class="menu-item">
                                            <label>
                                                <input type="checkbox"/>
                                                <span class="check-icon"></span>
                                                民兵列表
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <ul class="group-list">
                                <!-- 根据style的top和left动态渲染定位 -->
                                <li class="group-item" style="top: 20%; left: 40%">
                                    民兵组织1
                                </li>
                                <li class="group-item" style="top: 40%; left: 20%">
                                    民兵组织2
                                </li>
                                <li class="group-item" style="top: 20%; left: 20%">
                                    民兵组织3
                                </li>
                                <li class="group-item" style="top: 40%; left: 40%">
                                    民兵组织4
                                </li>
                                <li class="group-item" style="top: 60%; left: 60%">
                                    民兵组织5
                                </li>
                            </ul>
                        </div>
                    </form>
                </div>
                <div class="section-property">
                    <div class="block-introduction">
                        <div class="header">乡镇介绍</div>
                        <div class="content">
                            <p>周塔香是一个香塔，这里要打很多字很多字呢。
                                从前有座山，山上有座庙，庙里有个老和尚对小和尚讲故事说：
                                从前有座山，山上有座庙，庙里有个老和尚对小和尚讲故事说：
                                从前有座山，山上有座庙，庙里有个老和尚对小和尚讲故事说：
                                从前有座山，山上有座庙，庙里有个老和尚对小和尚讲故事说：
                                故事讲完了。
                                这就是尾递归。
                            </p>
                        </div>
                    </div>
                    <div class="block-soldiers">
                        <div class="header">乡镇民兵人员</div>
                        <div class="body">
                            <table class="table-soldiers">
                                <thead>
                                <tr>
                                    <th>姓名</th>
                                    <th>联系方式</th>
                                    <th>职位</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>紫小鲸</td>
                                    <td>13800138000</td>
                                    <td>指挥员</td>
                                    <td>
                                        <a class="btn btn-call"
                                           href="tel:13800138000"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>紫小鲸</td>
                                    <td>13800138000</td>
                                    <td>指挥员</td>
                                    <td>
                                        <a class="btn btn-call"
                                           href="tel:13800138000"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>紫小鲸</td>
                                    <td>13800138000</td>
                                    <td>指挥员</td>
                                    <td>
                                        <a class="btn btn-call"
                                           href="tel:13800138000"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>紫小鲸</td>
                                    <td>13800138000</td>
                                    <td>指挥员</td>
                                    <td>
                                        <a class="btn btn-call"
                                           href="tel:13800138000"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>紫小鲸</td>
                                    <td>13800138000</td>
                                    <td>指挥员</td>
                                    <td>
                                        <a class="btn btn-call"
                                           href="tel:13800138000"></a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
