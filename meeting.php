<?php include '_header.php'; ?>

    <div id="page" class="page-meeting">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">
                    发起临时会议
                    <a class="btn btn-close" href="javascript:"></a>
                </div>
                <div class="block-search">
                    <form action="">
                        <div class="search-box">
                            <input type="text" class="input-search" placeholder="输入姓名搜索"/>
                        </div>
                    </form>
                </div>
                <div class="block-content">
                    <div class="block-list">
                        <div class="block-title">民兵列表</div>
                        <ul class="soldier-list">
                            <li class="soldier-item selected">
                                <a href="javascript:">王昭君</a>
                            </li>
                            <li class="soldier-item">
                                <a href="javascript:">程咬金</a>
                            </li>
                            <li class="soldier-item">
                                <a href="javascript:">甄姬</a>
                            </li>
                            <li class="soldier-item">
                                <a href="javascript:">田馥甄</a>
                            </li>
                        </ul>
                    </div>
                    <div class="block-action">
                        <ul class="menu-list">
                            <li class="menu-item">
                                <a class="btn btn-add" href="javascript:">添加&nbsp;&nbsp;&gt;</a>
                            </li>
                            <li class="menu-item">
                                <a class="btn btn-delete" href="javascript:">删除&nbsp;&nbsp;&lt;</a>
                            </li>
                        </ul>
                    </div>
                    <div class="block-selected">
                        <div class="block-title">已选成员</div>
                        <ul class="soldier-list">
                            <li class="soldier-item">
                                <div class="row">
                                    <div class="item item-name">
                                        <span class="label">姓名</span>
                                        王昭君
                                    </div>
                                    <div class="item item-title">
                                        <span class="label">职位</span>
                                        指挥长
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="item item-tel">
                                        <span class="label">电话</span>
                                        13800138000
                                    </div>
                                </div>
                            </li>
                            <li class="soldier-item">
                                <div class="row">
                                    <div class="item item-name">
                                        <span class="label">姓名</span>
                                        王昭君
                                    </div>
                                    <div class="item item-title">
                                        <span class="label">职位</span>
                                        指挥长
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="item item-tel">
                                        <span class="label">电话</span>
                                        13800138000
                                    </div>
                                </div>
                            </li>
                            <li class="soldier-item">
                                <div class="row">
                                    <div class="item item-name">
                                        <span class="label">姓名</span>
                                        王昭君
                                    </div>
                                    <div class="item item-title">
                                        <span class="label">职位</span>
                                        指挥长
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="item item-tel">
                                        <span class="label">电话</span>
                                        13800138000
                                    </div>
                                </div>
                            </li>
                            <li class="soldier-item">
                                <div class="row">
                                    <div class="item item-name">
                                        <span class="label">姓名</span>
                                        王昭君
                                    </div>
                                    <div class="item item-title">
                                        <span class="label">职位</span>
                                        指挥长
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="item item-tel">
                                        <span class="label">电话</span>
                                        13800138000
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="row-action">
                            <a class="btn btn-start" href="javascript:">发起会议</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="dialog">
                <div class="dialog-window">
                    <div class="dialog-header">
                        发起临时会议
                        <a class="btn btn-close" href="javascript:">&times;</a>
                    </div>
                    <div class="dialog-body">
                        <div class="block-video">
                            <div class="participants">
                                <ul class="user-list">
                                    <li class="user-item active">
                                        <!-- 动态修改backgroundImage以设定头像 -->
                                        <a href="javascript:"
                                           style="background-image: url()"></a>
                                        <div class="label">甄姬视频中</div>
                                    </li>
                                    <li class="user-item connecting">
                                        <!-- 动态修改backgroundImage以设定头像 -->
                                        <a href="javascript:"></a>
                                        <div class="label">与昭君连线中</div>
                                    </li>
                                    <li class="user-item exit">
                                        <!-- 动态修改backgroundImage以设定头像 -->
                                        <a href="javascript:"
                                           style="background-image: url()"></a>
                                        <div class="full-message">
                                            <div class="row">詹姆斯</div>
                                            <div class="row">已退出</div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                            <video></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
