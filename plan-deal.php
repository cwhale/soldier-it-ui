<?php include '_header.php'; ?>

    <div id="page" class="page-plan-deal">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">应急预案</div>
                <div class="section-content">
                    <div class="section-filter">
                        <div class="section-title">应急行动小组设置</div>
                        <div class="block-selector block-title">
                            <ul class="list">
                                <li class="item">
                                    <label>
<!--                                        <input type="checkbox"/>-->
<!--                                        <span class="check-icon"></span>-->
                                        指挥部
                                    </label>
                                    <ul class="submenu">
                                        <li class="subitem selected">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                指挥长
                                            </label>
                                        </li>
                                        <li class="subitem">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                副指挥长
                                            </label>
                                        </li>
                                        <li class="subitem">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                相关人员
                                            </label>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="block-selector block-group">
                            <ul class="list">
                                <li class="item">
                                    <label>
<!--                                        <input type="checkbox"/>-->
<!--                                        <span class="check-icon"></span>-->
                                        应急处置小组
                                    </label>
                                    <ul class="submenu">
                                        <li class="subitem">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                现场搜救组
                                            </label>
                                        </li>
                                        <li class="subitem">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                现场检测组
                                            </label>
                                        </li>
                                        <li class="subitem">
                                            <label>
<!--                                                <input type="checkbox"/>-->
<!--                                                <span class="check-icon"></span>-->
                                                新闻发布组
                                            </label>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="section-detail">
                        <div class="section-title" style="width: 240px">小组成员</div>
                        <div class="block-soldiers">
                            <ul class="soldier-list">
                                <li class="soldier-item">
                                    <label class="check-widget">
                                        <input type="checkbox"/>
                                        <span class="icon"></span>
                                    </label>
                                    <div class="row">
                                        <div class="item item-name">
                                            <span class="label">姓名</span>
                                            王昭君
                                        </div>
                                        <div class="item item-title">
                                            <span class="label">职位</span>
                                            指挥长
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="item item-tel">
                                            <span class="label">电话</span>
                                            13800138000
                                        </div>
                                    </div>
                                </li>
                                <li class="soldier-item">
                                    <label class="check-widget">
                                        <input type="checkbox"/>
                                        <span class="icon"></span>
                                    </label>
                                    <div class="row">
                                        <div class="item item-name">
                                            <span class="label">姓名</span>
                                            王昭君
                                        </div>
                                        <div class="item item-title">
                                            <span class="label">职位</span>
                                            指挥长
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="item item-tel">
                                            <span class="label">电话</span>
                                            13800138000
                                        </div>
                                    </div>
                                </li>
                                <li class="soldier-item">
                                    <label class="check-widget">
                                        <input type="checkbox"/>
                                        <span class="icon"></span>
                                    </label>
                                    <div class="row">
                                        <div class="item item-name">
                                            <span class="label">姓名</span>
                                            王昭君
                                        </div>
                                        <div class="item item-title">
                                            <span class="label">职位</span>
                                            指挥长
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="item item-tel">
                                            <span class="label">电话</span>
                                            13800138000
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="block-sms">
                            <div class="block-title">短信通知模板</div>
                            <div class="block-content">
                                <textarea class="input" title></textarea>
                            </div>
                        </div>
                        <div class="block-action">
                            <a href="javascript:" class="btn btn-save">保存</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php include '_footer.php';
