<?php include '_header.php'; ?>

    <div id="page" class="page-plan-edit">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">应急预案</div>
                <div class="section-content">
                    <div class="row-label">
                        <div class="label">处理步骤</div>
                    </div>
                    <div class="section section-plan">
                        <form action="">
                            <div class="block-class">
                                <div class="block-header">类别</div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-level">
                                <div class="block-header">分级标准</div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-step">
                                <div class="block-header">第1步</div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="block-header">
                                    内容
                                    <a class="btn btn-delete" href="javascript:">删除</a>
                                    <a class="link link-deal">设置应急行动小组及短信通知模板</a>
                                </div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-step">
                                <div class="block-header">第2步</div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="block-header">
                                    内容
                                    <a class="btn btn-delete" href="javascript:">删除</a>
                                    <a class="link link-deal">设置应急行动小组及短信通知模板</a>
                                </div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-action">
                                <a class="btn btn-add" href="javascript:">新增步骤</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

<?php include '_footer.php';
