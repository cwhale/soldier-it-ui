<?php include '_header.php'; ?>

    <div id="page" class="page-plan">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">应急预案</div>
                <div class="section-content">
                    <div>
                        <div class="block-actions-menu">
                            <span class="label">类型</span>
                            <a class="btn btn-add" href="javascript:"></a>
                            <a class="btn btn-delete" href="javascript:"></a>
                            <a class="btn btn-edit" href="javascript:"></a>
                            <a class="btn btn-reload" href="javascript:"></a>
                        </div>
                        <div class="block-actions-menu">
                            <span class="label">预案</span>
                            <a class="btn btn-add" href="javascript:"></a>
                            <a class="btn btn-delete" href="javascript:"></a>
                            <a class="btn btn-edit" href="javascript:"></a>
                            <a class="btn btn-reload" href="javascript:"></a>
                        </div>
                    </div>
                    <div class="block-classification">
                        <ul class="class-list">
                            <li class="class-item">
                                <label>
                                    <input type="checkbox"/>
                                    <span class="check-icon"></span>
                                    自然灾害
                                </label>
                                <ul class="submenu">
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            防汛应急
                                        </label>
                                    </li>
                                    <!-- 通过 active 类控制 -->
                                    <li class="class-subitem active">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            旱灾
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            森林火灾
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            地雷
                                        </label>
                                    </li>
                                </ul>
                            </li>
                            <li class="class-item">
                                <label>
                                    <input type="checkbox"/>
                                    <span class="check-icon"></span>
                                    事故灾难
                                </label>
                                <ul class="submenu">
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            交通道路事故
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            旅游突发事故
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            水上救援
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            煤矿事故
                                        </label>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="block-name">
                        <div class="block-title">预案名称</div>
                        <div class="block-content">
                            <ul class="list">
                                <li class="list-item active">
                                    <a href="javascript:">防汛应急预案</a>
                                </li>
                                <li class="list-item">
                                    <a href="javascript:">防汛应急预案</a>
                                </li>
                                <li class="list-item">
                                    <input type="text" placeholder="输入预案名称"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="block-deal">
                        <div class="row-label">
                            <div class="label">处理步骤</div>
                        </div>
                        <div class="section section-plan">
                            <form action="">
                                <div class="block-class">
                                    <div class="block-header">类别</div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-level">
                                    <div class="block-header">分级标准</div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-step">
                                    <div class="block-header">第1步</div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="block-header">
                                        内容
                                        <a class="btn btn-delete" href="javascript:">删除</a>
                                        <a class="link link-deal">设置应急行动小组及短信通知模板</a>
                                    </div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-step">
                                    <div class="block-header">第2步</div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-content">
                                    <div class="block-header">
                                        内容
                                        <a class="btn btn-delete" href="javascript:">删除</a>
                                        <a class="link link-deal">设置应急行动小组及短信通知模板</a>
                                    </div>
                                    <div class="block-body">
                                        <textarea class="block-input" title></textarea>
                                    </div>
                                </div>
                                <div class="block-action">
                                    <a class="btn btn-add" href="javascript:">新增步骤</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="dialog dialog-edit-plan">
            <div class="dialog-window">
                <div class="dialog-header">
                    新增预案类型
                </div>
                <div class="dialog-body">
                    <div class="row">
                        <div class="label">上级预案类型</div>
                        <div class="control">
                            <!-- 通过 expanded 类控制 -->
                            <div class="select expanded">
                                <div class="text">无上级</div>
                                <div class="dropdown">
                                    <ul class="dropdown-list">
                                        <li><a href="javascript:">无上级</a></li>
                                        <li><a href="javascript:">一级预案类型</a></li>
                                        <li><a href="javascript:">一级预案类型</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="label">预案类型名称</div>
                        <div class="control">
                            <input type="text" name="name" placeholder=""/>
                        </div>
                    </div>
                    <div class="row-action">
                        <a class="btn btn-cancel">取消</a>
                        <a class="btn btn-submit">确定</a>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
