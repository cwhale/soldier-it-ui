<?php include '_header.php'; ?>

    <div id="page" class="page-urgency">

        <div class="layer-background">

            <?php include '_masthead.php'; ?>

            <?php include '_sidebar.php'; ?>

            <div class="content with-sidebar">
                <div class="block-subtitle">险情处理</div>
                <div class="section-detail">
                    <div class="section section-event-list">
                        <table class="table-event">
                            <thead>
                            <tr>
                                <th>事件主题</th>
                                <th>类型</th>
                                <th>登记时间</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="active">
                                <td>富士山体滑坡</td>
                                <td>突发事件</td>
                                <td>2017/12/22 12:12:33</td>
                            </tr>
                            <tr>
                                <td>地震</td>
                                <td>突发事件</td>
                                <td>2017/12/22 12:12:33</td>
                            </tr>
                            <tr>
                                <td>地震</td>
                                <td>突发事件</td>
                                <td>2017/12/22 12:12:33</td>
                            </tr>
                            <tr>
                                <td>地震</td>
                                <td>突发事件</td>
                                <td>2017/12/22 12:12:33</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="section section-tab">
                        <!-- 通过active类控制高亮状态 -->
                        <div class="tab tab-detail active">
                            <a class="btn btn-detail">事件详情</a>
                        </div>
                        <div class="tab tab-plan">
                            <a class="btn btn-plan">应急预案</a>
                        </div>
                    </div>
                    <div class="section section-plan">
                        <form action="">
                            <div class="block-classify">
                                <div class="block-header">类别</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title>这里是类别，
类别</textarea>
                                </div>
                            </div>
                            <div class="block-level">
                                <div class="block-header">分级标准</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title></textarea>
                                </div>
                            </div>
                            <div class="block-step">
                                <div class="block-header">第1步</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title></textarea>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="block-header">内容</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title></textarea>
                                </div>
                            </div>
                            <div class="block-confirm-situation">
                                <div class="block-header">确认情况</div>
                                <div class="block-body">
                                    <ul class="confirm-list">
                                        <li class="confirm-item">
                                            <div class="col">人员：赵云；兰陵王</div>
                                            <div class="col">发送时间：2017-02-15 20:20:24</div>
                                            <div class="col">确认时间：2017-02-15 20:20:24</div>
                                        </li>
                                        <li class="confirm-item">
                                            <div class="col">人员：赵云；兰陵王</div>
                                            <div class="col">发送时间：2017-02-15 20:20:24</div>
                                            <div class="col">确认时间：2017-02-15 20:20:24</div>
                                        </li>
                                        <li class="confirm-item">
                                            <div class="col">人员：赵云；兰陵王</div>
                                            <div class="col">发送时间：2017-02-15 20:20:24</div>
                                            <div class="col">确认时间：2017-02-15 20:20:24</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="block-step">
                                <div class="block-header">第2步</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title></textarea>
                                </div>
                            </div>
                            <div class="block-content">
                                <div class="block-header">内容</div>
                                <div class="block-body">
                                    <textarea class="block-input" rows="2" title></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="section section-event-detail">
                        <form action="">
                            <div class="block-description">
                                <div class="block-header">事件描述</div>
                                <div class="block-body">
                                    <textarea class="block-input" title></textarea>
                                </div>
                            </div>
                            <div class="block-address">
                                <div class="block-header">发生地点</div>
                                <div class="block-body">
                                    <input type="text" class="block-input" title/>
                                </div>
                            </div>
                            <div class="block-user">
                                <div class="block-header">上报人员</div>
                                <div class="block-body">
                                    <div class="input-item">
                                        <div class="label">姓名：</div>
                                        <input type="text" class="block-input" title/>
                                    </div>
                                    <div class="input-item">
                                        <div class="label">电话：</div>
                                        <input type="text" class="block-input" title/>
                                    </div>
                                    <div class="input-item">
                                        <div class="label">职务：</div>
                                        <input type="text" class="block-input" title/>
                                    </div>
                                </div>
                            </div>
                            <div class="block-image">
                                <div class="block-header">现场图片</div>
                                <div class="block-body">
                                    <ul class="gallery">
                                        <li class="image-item">
                                            <img class="image"/>
                                        </li>
                                        <li class="image-item">
                                            <img class="image"/>
                                        </li>
                                        <li class="image-item">
                                            <img class="image"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="block-video">
                                <div class="block-header">现场视频</div>
                                <div class="block-body">
                                    <ul class="gallery">
                                        <li class="image-item">
                                            <img class="image"/>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="block-action">
                                <a class="btn btn-start" href="javascript:">启动预案</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="dialog">
                <div class="dialog-window">
                    <div class="block-classification">
                        <ul class="class-list">
                            <li class="class-item">
                                <label>
<!--                                    <input type="radio"/>-->
                                    自然灾害
<!--                                    <span class="check-icon"></span>-->
                                </label>
                                <ul class="submenu">
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            防汛应急
                                        </label>
                                    </li>
                                    <li class="class-subitem active">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            旱灾
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            森林火灾
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            地雷
                                        </label>
                                    </li>
                                </ul>
                            </li>
                            <!-- 利用 collapsed 标签控制下拉大小 -->
                            <li class="class-item collapsed">
                                <label>
<!--                                    <input type="radio"/>-->
                                    事故灾难
<!--                                    <span class="check-icon"></span>-->
                                </label>
                                <ul class="submenu">
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            交通道路事故
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            旅游突发事故
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            水上救援
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            煤矿事故
                                        </label>
                                    </li>
                                    <li class="class-subitem">
                                        <label>
<!--                                            <input type="radio"/>-->
<!--                                            <span class="check-icon"></span>-->
                                            煤矿事故
                                        </label>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="block-name">
                        <div class="window">
                            <div class="block-title">预案名称</div>
                            <div class="block-content">
                                <ul class="list">
                                    <li class="list-item active">
                                        <a href="javascript:">防汛应急预案</a>
                                    </li>
                                    <li class="list-item">
                                        <a href="javascript:">防汛应急预案</a>
                                    </li>
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
<!--                                    <li class="list-item">-->
<!--                                        <a href="javascript:">防汛应急预案</a>-->
<!--                                    </li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

<?php include '_footer.php';
